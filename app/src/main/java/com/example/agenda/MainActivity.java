package com.example.agenda;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    EditText edtNombre;
    EditText edtTelefono;
    EditText edtTelefono2;
    EditText edtDireccion;
    EditText edtNotas;
    CheckBox cbxFavorito;
    Contacto saveContact;
    int savedIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final ArrayList<Contacto> contactos = new ArrayList<Contacto>();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtNombre = (EditText) findViewById(R.id.edtNombre);
        edtTelefono = (EditText) findViewById(R.id.edtTelefono1);
        edtTelefono2 = (EditText) findViewById(R.id.edtTelefono2);
        edtDireccion = (EditText) findViewById(R.id.edtDireccion);
        edtNotas = (EditText) findViewById(R.id.edtNotas);
        cbxFavorito = (CheckBox) findViewById(R.id.chkFavorito);
        Button btnGuardar = (Button) findViewById(R.id.btnGuardar);
        Button btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        Button btnListar = (Button) findViewById(R.id.btnListar);
        saveContact = null;
        savedIndex = 0;
        btnGuardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtNombre.getText().toString().equals("") || edtDireccion.getText().toString().equals("") || edtTelefono.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, R.string.mensajeerror, Toast.LENGTH_SHORT).show();
                } else {
                    Contacto nContacto = new Contacto();
                    int index = contactos.size();
                    if (saveContact != null) {
                        contactos.remove(savedIndex);
                        nContacto = saveContact;
                        index = savedIndex;
                    }
                    nContacto.setNombre(edtNombre.getText().toString());
                    nContacto.setTelefono1(edtTelefono.getText().toString());
                    nContacto.setTelefono2(edtTelefono2.getText().toString());
                    nContacto.setDireccion(edtDireccion.getText().toString());
                    nContacto.setNotas(edtNotas.getText().toString());
                    nContacto.setFavorite(cbxFavorito.isChecked());
                    contactos.add(index, nContacto);
                    Toast.makeText(MainActivity.this, R.string.mensaje, Toast.LENGTH_SHORT).show();
                    saveContact = null;
                    limpiar();
                }
            }
        });
        btnListar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity.this, ListaActivity.class);
                Bundle bObject = new Bundle();
                bObject.putSerializable("contactos", contactos);
                i.putExtras(bObject);
                startActivityForResult(i, 0);
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (intent != null) {
            Bundle oBundle = intent.getExtras();
            saveContact = (Contacto) oBundle.getSerializable("contacto");
            savedIndex = oBundle.getInt("index");
            edtNombre.setText(saveContact.getNombre());
            edtTelefono.setText(saveContact.getTelefono1());
            edtTelefono2.setText(saveContact.getTelefono2());
            edtDireccion.setText(saveContact.getDireccion());
            edtNotas.setText(saveContact.getNotas());
            cbxFavorito.setChecked(saveContact.isFavorite());
        } else {
            limpiar();
        }

    }

    public void limpiar() {
        saveContact = null;
        edtNombre.setText("");
        edtTelefono.setText("");
        edtTelefono2.setText("");
        edtDireccion.setText("");
        edtNotas.setText("");
        cbxFavorito.setChecked(false);
    }
}
